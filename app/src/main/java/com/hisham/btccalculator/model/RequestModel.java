package com.hisham.btccalculator.model;

import java.io.Serializable;

/**
 * Created by Hisham on 30/Nov/2017 - 17:47
 */

public class RequestModel implements Serializable {
    private final String highLink = "https://api.coinsecure.in/v1/exchange/bid/high";
    private final String lowLink = "https://api.coinsecure.in/v1/exchange/ask/low";
    private final String cryptoLink = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,INR";
    private final String zebPayLink = "https://www.zebapi.com/api/v1/market/ticker/btc/inr";

    public String getHighLink() {
        return highLink;
    }

    public String getLowLink() {
        return lowLink;
    }

    public String getCryptoLink() {
        return cryptoLink;
    }

    public String getZebPayLink() {
        return zebPayLink;
    }
}
