package com.hisham.btccalculator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hisham on 10/Dec/2017 - 17:42
 */

public class CryptoCompareModel implements Serializable {
    // for crypto compare
    @SerializedName("USD")
    @Expose
    private Double uSD;
    @SerializedName("INR")
    @Expose
    private Double iNR;

    public Double getUSD() {
        return uSD;
    }

    public void setUSD(Double uSD) {
        this.uSD = uSD;
    }

    public Double getINR() {
        return iNR;
    }

    public void setINR(Double iNR) {
        this.iNR = iNR;
    }


}
