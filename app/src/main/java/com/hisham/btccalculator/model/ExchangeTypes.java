package com.hisham.btccalculator.model;

/**
 * Created by Hisham on 10/Dec/2017 - 17:44
 */

public enum ExchangeTypes {
    COINSECURE, ZEBPAY, CRYPTOCOMPARE
}
