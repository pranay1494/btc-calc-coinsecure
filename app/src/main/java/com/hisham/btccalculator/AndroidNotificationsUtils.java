package com.hisham.btccalculator;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

/**
 * Created by Hishu on 6/9/2016.
 */

public class AndroidNotificationsUtils {


    public void sendGeneralNotification(int id, Context context, String messageBody, String title, boolean isVibrate) {
        Intent intent = new Intent(context, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // create notification
        NotificationCompat.Builder notificationBuilder = buildNotification(context, messageBody, title, intent, isVibrate);

        // fire notification
        fireNotification(context, notificationBuilder, id);
    }

    private static final String TAG = AndroidNotificationsUtils.class.getSimpleName();
    private NotificationCompat.Builder buildNotification(Context context, String messageBody, String title, Intent intent, boolean isVibrate) {

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent to the top of the stack
        Intent parentIntent = new Intent(context, MainActivity.class);
        stackBuilder.addNextIntent(parentIntent);
        stackBuilder.addNextIntent(intent);
        Log.d(TAG, "buildNotification: count: " + stackBuilder.getIntentCount());
        Log.d(TAG, "buildNotification: intents : " + stackBuilder.getIntents());

//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = {500, 500, 500, 500, 500};
        //final PendingIntent pendingIntent = PendingIntent.getActivities(context, 0, new Intent[] {parentIntent, intent}, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);//PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
               builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true);

               if(isVibrate) {
                   builder.setVibrate(pattern);

               }
               builder.setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent);
        return builder;
    }

    private void fireNotification(Context context, NotificationCompat.Builder notificationBuilder, int notificationID) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notificationBuilder.build());
    }
}
