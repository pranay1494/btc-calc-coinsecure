package com.hisham.btccalculator.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.hisham.btccalculator.AndroidNotificationsUtils;
import com.hisham.btccalculator.AppSharedPrefs;
import com.hisham.btccalculator.MainActivity;
import com.hisham.btccalculator.R;
import com.hisham.btccalculator.model.CryptoCompareModel;
import com.hisham.btccalculator.model.ExchangeTypes;
import com.hisham.btccalculator.model.RequestModel;
import com.hisham.btccalculator.model.CoinsecureModel;
import com.hisham.btccalculator.model.ZebPayModel;

import org.apache.commons.math3.util.Precision;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloaderService extends Service {

    private static final String TAG = DownloaderService.class.getSimpleName();

    private NotificationCompat.Builder builder;
    private AppSharedPrefs appSharedPrefs;
    private String bigStyleText = "";

    public DownloaderService() {
        appSharedPrefs = AppSharedPrefs.getInstance(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        PendingIntent dismissPendingIntent = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(),
                new Intent(this, StopServiceReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);

        builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Polling Coinsecure & CryptoCompare")
                .setContentIntent(pendingIntent);
        builder.addAction(R.drawable.ic_cancel_black_24dp, "Stop", dismissPendingIntent);
        Notification notification = builder.build();
        startForeground(1337, notification);
    }

    private boolean cancelled = false; // means cancel everything
    private boolean isDownloading = false;
    private Thread thread;
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra("RequestModel")) {
            if(thread == null || !thread.isAlive()) { // if thread is not running, start it. to maintain only one background thread is running at all time.
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (!isDownloading) {
                            isDownloading = true;
                            while (isDownloading) {
                                RequestModel requestModel = (RequestModel) intent.getSerializableExtra("RequestModel");
                                startDownload(requestModel.getHighLink(), ExchangeTypes.COINSECURE);
                                startDownload(requestModel.getLowLink(), ExchangeTypes.COINSECURE);
                                startDownload(requestModel.getCryptoLink(), ExchangeTypes.CRYPTOCOMPARE);
                                startDownload(requestModel.getZebPayLink(), ExchangeTypes.ZEBPAY);
                                try {
                                    Thread.sleep(appSharedPrefs.getCallTimer()*1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
                thread.start();
            }

            Log.d(TAG, "Thread Information: " + thread.toString());

        }
        return START_REDELIVER_INTENT;
    }

    private void stopEverything(boolean fromDestroy) {
        cancelled = true;
        isDownloading = false;
        if(!fromDestroy) {
            stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String startDownload(String link, ExchangeTypes exchangeTypes) {
        InputStream input = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(link);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            input = connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }

            if (isCancelled()) {
                input.close();
                return null;
            }
            sendCallback(total.toString(), exchangeTypes);
        } catch (Exception e) {
            sendCallback(null, exchangeTypes);
            return e.toString();
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException ignored) { }
            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    int counter = 0;
    private void sendCallback(String data, ExchangeTypes exchangeType) {
    counter++;
    Intent i = new Intent("progress_callback");
    boolean isNotification = appSharedPrefs.getBooleanPref("Notification");
    i.putExtra("exchangeType", exchangeType);

    if(exchangeType == ExchangeTypes.COINSECURE){
        CoinsecureModel coinsecureModel = new Gson().fromJson(data, CoinsecureModel.class);
         i.putExtra("coinsecureModel", coinsecureModel);
        if (coinsecureModel != null ) {
            double csInrDiff = Double.parseDouble(appSharedPrefs.getINRDiff());
            if ("Lowest Ask".equalsIgnoreCase(coinsecureModel.getMethod())) {
                Double csBuy = coinsecureModel.getMessage().getRate() / 100;
                Double buy = Double.valueOf(appSharedPrefs.getStringPref("buy"));
                if(buy == null || buy == 0){
                    buy = csBuy;
                    appSharedPrefs.saveStringPreference("buy" , ""+buy);
                } else if (Math.abs(csBuy - buy) > csInrDiff && isNotification) {
                    (new AndroidNotificationsUtils()).sendGeneralNotification(5, this, "Buy value changed ("+ Precision.round((csBuy - buy),2)+") from \u20B9 " + buy + " to \u20B9 " + csBuy, "CoinSecure", appSharedPrefs.getBooleanPref("Vibration"));
                    appSharedPrefs.saveStringPreference("buy" , ""+csBuy);
                }
                bigStyleText = bigStyleText + " CS BUY: \u20B9 " + csBuy + System.getProperty("line.separator");
            } else if ("Highest Bid".equalsIgnoreCase(coinsecureModel.getMethod())) {
                Double csSell = coinsecureModel.getMessage().getRate() / 100;
                Double sell = Double.valueOf(appSharedPrefs.getStringPref("sell"));
                if(sell == null || sell == 0){
                    sell = csSell;
                    appSharedPrefs.saveStringPreference("sell" , ""+sell);
                } else if (Math.abs(csSell - sell) > csInrDiff && isNotification) {
                    (new AndroidNotificationsUtils()).sendGeneralNotification(6, this, "Buy value changed ("+ Precision.round((csSell - sell),2)+") from \u20B9 " + sell + " to \u20B9 " + csSell, "CoinSecure", appSharedPrefs.getBooleanPref("Vibration"));
                    appSharedPrefs.saveStringPreference("sell" , ""+csSell);
                }
                bigStyleText = bigStyleText + " CS SELL: \u20B9 " + csSell + System.getProperty("line.separator");
            }
        }

    } else if(exchangeType == ExchangeTypes.CRYPTOCOMPARE) {
        CryptoCompareModel cryptoCompareModel = new Gson().fromJson(data, CryptoCompareModel.class);
        i.putExtra("cryptoCompareModel", cryptoCompareModel);

        bigStyleText = bigStyleText + "Crypto : $ " + cryptoCompareModel.getUSD() + System.getProperty("line.separator");
        bigStyleText = bigStyleText + "Crypto : \u20B9 " + cryptoCompareModel.getINR() + System.getProperty("line.separator");

        Double cryptoUSD = cryptoCompareModel.getUSD();
        Double usd = Double.valueOf(appSharedPrefs.getStringPref("USD"));
        if(usd == null || usd == 0){
            usd = cryptoUSD;
            appSharedPrefs.saveStringPreference("USD" , ""+usd);
        } else if (Math.abs(cryptoUSD - usd) > Double.parseDouble(appSharedPrefs.getDollarDiff()) && isNotification) {
            String messageBody = "USD value changed (" + Precision.round((cryptoUSD - usd),2) + ") from $ " + usd + " to $ " + cryptoUSD;
            (new AndroidNotificationsUtils()).sendGeneralNotification(9, this, messageBody, "CryptoCompare", appSharedPrefs.getBooleanPref("Vibration"));
            appSharedPrefs.saveStringPreference("USD" , ""+cryptoUSD);
        }

    } else if(exchangeType == ExchangeTypes.ZEBPAY) {
        ZebPayModel zebPayModel = new Gson().fromJson(data, ZebPayModel.class);
        i.putExtra("zebPayModel", zebPayModel);

        bigStyleText = bigStyleText + "Zebpay Buy: \u20B9 " + zebPayModel.getBuy() + System.getProperty("line.separator");
        bigStyleText = bigStyleText + "Zebpay Sell : \u20B9 " + zebPayModel.getSell() + System.getProperty("line.separator");

        Double zebPayBuy = zebPayModel.getBuy();
        Double zebPaySell = zebPayModel.getSell();
        Double buy = Double.valueOf(appSharedPrefs.getStringPref("zebPayBuy"));
        Double sell = Double.valueOf(appSharedPrefs.getStringPref("zebPaySell"));
        if(buy == null || buy == 0){
            buy = zebPayBuy;
            appSharedPrefs.saveStringPreference("zebPayBuy" , ""+zebPayBuy);
        }

        if(sell == null || sell == 0){
            sell = zebPaySell;
            appSharedPrefs.saveStringPreference("zebPaySell" , ""+zebPaySell);
        }

        if (Math.abs(zebPayBuy - buy) > Double.parseDouble(appSharedPrefs.getINRDiff()) && isNotification) {
            String messageBody = "ZebPay buy value changed (" + Precision.round((zebPayBuy - buy),2) + ") from \u20B9 "
                    + buy + " to \u20B9 " + zebPayBuy;
            (new AndroidNotificationsUtils()).sendGeneralNotification(8, this, messageBody, "ZebPay", appSharedPrefs.getBooleanPref("Vibration"));
            appSharedPrefs.saveStringPreference("zebPayBuy" , ""+zebPayBuy);
        }

        if (Math.abs(zebPaySell - sell) > Double.parseDouble(appSharedPrefs.getINRDiff()) && isNotification) {
            String messageBody = "ZebPay sell value changed (" + Precision.round((zebPaySell - sell),2) + ") from \u20B9 "
                    + sell + " to \u20B9 " + zebPaySell;
            (new AndroidNotificationsUtils()).sendGeneralNotification(7, this, messageBody, "ZebPay", appSharedPrefs.getBooleanPref("Vibration"));
            appSharedPrefs.saveStringPreference("zebPaySell" , ""+zebPaySell);
        }

    }

        sendBroadcast(i);



        // update noti
        builder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(bigStyleText));
        Notification notification = builder.build();
        startForeground(1337, notification);

        if(counter % 4 == 0){
            bigStyleText = "";
        }
    }


    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopEverything(true);
    }
}
